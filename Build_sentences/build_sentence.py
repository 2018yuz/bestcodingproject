import random
from nltk.corpus import wordnet

def fill_blank(noun,number):
    input_word = noun + ".n.01"
    sentence= wordnet.synset(input_word).examples()
    return sentence

def fill_blank_otherway(noun,number):
    '''
        The fonction is used in the condition where the keywords can not be found in the NTLK library. We set up four module of sentences
        and randomly choose one to form the sentence with keywords. The grammar is considered completely.
    :param noun: list
    :param number: list
    :return: sentence:string
    '''
    choose = random.randint(0,3)
    sentence = ""
    if choose == 0:
        sentence = "I like this photo of the #{}".format(noun[0])
        if number[0]=='P':
            sentence += 's'
        if len(noun)>1:
          for i in range(1,len(noun)):
           sentence += " ,#{}".format(noun[i])
           if number[i] =='P':
                 sentence += 's'
        sentence += '.'
    if choose == 1:
        sentence= "Today, I see #{}".format(noun[0])
        if number[0]=='P':
            sentence += 's.  '
        else:
            sentence += '.  '
        if len(noun)>1:
          sentence += " There is(are) also "
          for i in range(1,len(noun)):
              sentence += "#{}".format(noun[i])
              if number[i] =='P':
                  sentence += 's '
          sentence += '.'
    if choose == 2:
        if len(noun)==1 and number[0]=='S':
            sentence = "There is the #{}.".format(noun[0])
        else:
            sentence = "There are  #{}".format(noun[0])
        if number[0] =='P':
            sentence += "s"
        if len(noun) == 1:
            sentence += "."
        else:
            for i in range(1, len(noun)):
                sentence += "  #{}".format(noun[i])
                if number[i] =='P':
                    sentence += 's '
            sentence += '.'
    if choose == 3:
        if len(noun)==1 and number[0]=='S':
            sentence = "What a nice #{} !".format(noun[0])
        else:
            if len(noun)==1:
                sentence="What nice #{}s !".format(noun[0])
            else:
                sentence = "What nice"
                for i in range(0,len(noun)):
                    sentence += " {}".format(noun[i])
                    if number[i] =='P':
                        sentence += "s"
                sentence += "!"
    return sentence










from Build_sentence.generate_sentence import *
import pytest


def test_default_generating_sentence():
    assert ("friend" or "ties" in generatingsentence(["person", "tie"],["S", "P"],["green", "blue"]))

def test_default_generating_from_image():
    assert isinstance(generating_from_images({'Image_Trump_0': [['air plane', 'tie', 'car', 'person'], ['S', 'P', 'S', 'P'], ['gray', 'blue', 'gray', 'gray']]}),dict)


def test_default_is_singular():
    assert if_singular("P","cat") == "cats"


pytest
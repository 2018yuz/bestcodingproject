import shutil
import os
import cv2
from imageai.Detection import ObjectDetection
from Collect_image.collect_tweets import *
from Recognise_image.color_recogn_hsv import get_color



def get_Keyword(key_term,image_number):
    """
    Input the search keyword and the numbers of the images, return the objects, number and the color
    :param key_term: string,
    :param image_number:
    :return: a dictionary with the key : "Image_[key_term]_n" ; with the value: [name, plural, color]
           name is a list of string, plural is a list of "S" or "P", color is a lst of string
    """
    #output_Dataframe = pd.DataFrame(columns=["Img_Name", "Object", "Number"])
    #output_Dataframe_Description = pd.DataFrame(columns=["Img_Name", "Object", "Color"])
    output_data= {}
    for i in range(image_number):   #extract keywords from each image
        detections = detector(i, key_term, 50)
        # For output_Dataframe_Description
        (name_temp,color_temp) = get_name_and_color(detections)

        object_set= set(name_temp)
        color_dict = {}

        name = list(object_set)
        color = []
        plural = []

        for elem in object_set:
            plural.append(plu(elem,name_temp))
            for k in range(len(name_temp)) :
                if name_temp[k] == elem:
                    color_dict[color_temp[k]]= color_dict.get(color_temp[k],0)+ 1
# If the same objects appears several times in different colors, we calculate the most frequent color and consider it to be the color of the object.
            color.append(max(color_dict, key=color_dict.get))

        output_data["Image_"+key_term+"_"+str(i)]= [name, plural, color]
        print("The " + str(i+1) +" / " + str(image_number) + " image has been successfully processed")
    shutil.rmtree(os.getcwd() + "//-objects")
    #print(output_data)
    return output_data

def detector(i, key_term, min_proba):
    '''
    Set up and detect objects from the input image. It will capture the objects in the image and store them.
    :param i: image number
    :param key_term: name of the folder
    :param min_proba: we consider the object is in the picture when the probabilty is higher than min_proba
    :return: list containing the object name, the probability of it being in the picture and the path of the image
    '''
    detector = ObjectDetection()
    detector.setModelTypeAsRetinaNet()  # set the model type to RetinaNet
    detector.setModelPath(os.path.join(os.getcwd(),
                                       "resnet50_coco_best_v2.0.1.h5"))  # set the model path to the path of our RetinaNet model
    detector.loadModel()  # load the model into the object detection class
    detections = detector.detectObjectsFromImage(
        input_image=os.path.join(os.getcwd(), key_term + "//Image_" + key_term + "_" + str(i) + ".jpg"),
        minimum_percentage_probability=min_proba, extract_detected_objects=True)
    return detections

def get_name_and_color(detections):
    '''
    Recognition machine we have set up
    :param detections: list
    :return: tuple: One list containing the names, one list containing the colors
    '''
    object_name_temp = []
    object_color_temp = []
    jpg_n = 0
    for eachObject in detections[0]:
        # get color of each object and store it in list object_color

        image = cv2.imread(os.path.join(os.getcwd(), detections[1][jpg_n]))
        object_name_temp.append(eachObject["name"])
        object_color_temp.append(get_color(image))
        jpg_n += 1
    return (object_name_temp, object_color_temp)


def plu(elem, name_temp):
    '''
    Give the object a tag: plural or singular
    :param elem: an object (string)
    :param name_temp: list of object names (list of strings)
    :return: string "S" or "P" to represent plural or singular
    '''
    if name_temp.count(elem) == 1:
        return "S"
    else:
        return "P"
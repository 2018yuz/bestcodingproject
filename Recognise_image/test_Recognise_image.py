from Recognise_image.color_recogn_hsv import *
import pytest


def test_default_get_color():
    image = cv2.imread(os.path.join(os.getcwd(),"Image_Trump_0.jpg"))
    assert isinstance(get_color(image), str)


def test_is_an_image_get_color():
    non_image = 'test_doc.txt'
    with pytest.raises(TypeError):
       get_color(non_image)

def test_image_exists_get_color():
    with pytest.raises(TypeError):
       get_color()


if __name__ == '__main__':
    pytest.main('test_Recognise_image.py')
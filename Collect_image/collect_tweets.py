import tweepy
# We import our access keys:
from Build_sentences.appkeys import *

import os
import urllib.request

def twitter_setup():
    """
    Utility function to setup the Twitter's API
    with an access keys provided in a file appkeys.py
    :return: the authentified API
    """
    # Authentication and access using keys:
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)

    # Return API with authentication:
    api = tweepy.API(auth)
    return api



def collect_by_user_name(user_name):
    '''
    returns the tweets of a certain user by the user_id
    :param user_name: a string
    :return: tweets in json form
    '''
    if not isinstance(user_name,str):
        raise TypeError('Please provide a string argument')

    connexion = twitter_setup()
    return connexion.user_timeline(screen_name = user_name, count = 200)

def collectby(keyword):
    '''
    return the tweets that contain the keyword
    :param keyword: string
    :return: tweets in json form
    '''

    if not isinstance(keyword,str):
        raise TypeError('Please provide a string argument')

    connexion = twitter_setup()
    return connexion.user_timeline(keyword, count = 200)


def Creating_folder(name):
    '''
    Creates a folder in this location, to store the images
    :param name: string: name of the folder
    :return: path to the folder, where images can be saved
    '''
    path = os.getcwd() + "//{}".format(name)

    if not os.path.exists(path):
        os.makedirs(path)
    return path



def Collect_image_by_username(user_name):
    '''
    Saves the images tweeted by the user_name on this file
    :param user_name: string
    :return: the number of images
    '''
    status = collect_by_user_name(user_name)
    k=0 #Counting the images to name them, and to make a loop in the next functions

    if not isinstance(user_name,str):
        raise TypeError('Please provide a string argument')

    for tweet in status:
        try:
            for elem in tweet.entities['media']:
                path = Creating_folder(user_name)
                urllib.request.urlretrieve(elem['media_url_https'], path + "/Image_{}_{}.jpg".format(user_name,k))
                k+=1
                print("The "+str(k) + "th image has been successfully downloaded")
                if k == 10: # The maximum number of the image is 10
                    return k

        except: #Not all tweets have images. The try/except structure is more simple than using several "if" loops.
            continue
    return k #Number of images useful for the loop in the next module


def Collect_image_by_keyword(keyword):
    '''
    Saves the images that have the keyword in their description
    :param keyword: string
    :return: the number of images
    '''
    status = collectby(keyword)
    k=0 #Counting the images to name them
    if not isinstance(keyword,str):
        raise TypeError('Please provide a string argument')

    for tweet in status:
        try:
            for elem in tweet.entities['media']:
                path = Creating_folder(keyword)
                urllib.request.urlretrieve(elem['media_url_https'],path + "//Image_{}_{}.jpg".format(keyword,k))
                k+=1
                print("The " + str(k) + "th image has been successfully downloaded")
                if k == 10: # The maximum number of the image is 10
                    return k

        except: #Not all tweets have images.
            continue
    return k #Number of images useful for the loop in the next module


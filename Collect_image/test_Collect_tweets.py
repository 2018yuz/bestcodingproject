from Collect_image.Collect_tweets import *
import pytest


def test_default_Collect_image_by_username():
    assert Collect_image_by_username("EmmanuelMacron")>0

def test_raises_exception_on_non_string_arguments_username():
    with pytest.raises(TypeError):
        Collect_image_by_username(8)

def test_default_Collect_image_by_keyword():
    assert Collect_image_by_keyword("Christmas")>0

def test_raises_exception_on_non_string_arguments_keyword():
    with pytest.raises(TypeError):
        Collect_image_by_keyword(8)

pytest